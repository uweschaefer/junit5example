Simple example project that demonstrates a possible maven config for using JUnit5 Tags for categorizing Tests as Unit, Slow-Unit and Integration Tests and run them in the appropriate places.

'/infinitest.filters' configures [InfiniTest](https://infinitest.github.io/) accordingly.

Note that the container annotations included are just for convenience, and not required.

