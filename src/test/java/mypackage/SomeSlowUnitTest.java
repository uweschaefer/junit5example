package mypackage;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

// excludes testCase from infinitest, but not from surefire
@Tag("slow")
class SomeSlowUnitTest {

	@Test
	void testSlowTestSoShouldBeExcludedFromInfiniTest() throws Exception {
		System.out.println("Waiting one sec. This test is slow!");
		Thread.sleep(1000);
	}

}
