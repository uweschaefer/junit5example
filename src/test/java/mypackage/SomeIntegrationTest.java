package mypackage;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

//excludes whole testCase from infinitest and surefire
@Tag("it")
class SomeIntegrationTest {

	@Test
	void testOnlyRunInIntegration() {
	}
	
	@Test
	void testOnlyRunInIntegration2() {
	}

}
