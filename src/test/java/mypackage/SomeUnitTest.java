package mypackage;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import annotations.TestSlow;
import mypackage.UUT;

class SomeUnitTest {

	@Test
	void testUp() throws Exception {
		assertEquals("OINK", new UUT().upUpAndAway("oink"));
	}

	// excludes test from infinitest, but not from surefire
	@Tag("slow")
	@Test
	void excludedFromInfiniTest() throws InterruptedException {
		System.out.println("You can tag single tests as slow as well");
		Thread.sleep(600);
	}

	@TestSlow
	void alsoBeExcludedFromInfiniTest() throws InterruptedException {
		System.out.println("easier way to tag a test as slow");
		Thread.sleep(600);
	}



}
