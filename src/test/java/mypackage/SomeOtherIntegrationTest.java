package mypackage;

import annotations.TestIntegration;


class SomeOtherIntegrationTest {

	//exclude simple test from infinitest and surefire
	@TestIntegration
	void testOnlyRunInIntegration() {
	}

}
