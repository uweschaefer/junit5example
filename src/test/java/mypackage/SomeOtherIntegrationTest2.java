package mypackage;

import org.junit.jupiter.api.Test;

import annotations.TestIntegration;

@TestIntegration
class SomeOtherIntegrationTest2 {

	/**
	 * exclude simple test from infinitest and surefire, tag 'it' is contributed by the type annotation {@code TestIntegration}
	 */
	@Test
	void testOnlyRunInIntegration() {
		System.out.println("Running "+getClass().getSimpleName()+"::testOnlyRunInIntegration");
	}

}
