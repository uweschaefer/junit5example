package annotations;


import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
/**
 * Simple container annotation, just for convenience. Includes {@link Test} and {@code Tag} with value 'slow'.
 **/
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD,ElementType.TYPE})
// container for
@Test
@Tag("slow")
public @interface TestSlow {

}
