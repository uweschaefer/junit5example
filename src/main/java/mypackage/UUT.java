package mypackage;

public class UUT {

	public String upUpAndAway(String s) {
		if (s == null)
			throw new NullPointerException();
		
		return s.toUpperCase();
	}

}
